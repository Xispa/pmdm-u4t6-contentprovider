package dam.androidignacio.u4t6contacts;

import android.graphics.Bitmap;

public class ContactItem {

    String id;
    String name;
    String phoneNumber;
    Bitmap image;
    String contactId;
    String rawContactId;
    String lookUpKey;
    String phoneType;

    public ContactItem(String id, String name, String phoneNumber, Bitmap image, String contactId,
                       String rawContactId, String lookUpKey, String phoneType) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.contactId = contactId;
        this.rawContactId = rawContactId;
        this.lookUpKey = lookUpKey;
        this.phoneType = phoneType;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getContactId() {
        return contactId;
    }

    public String getRawContactId() {
        return rawContactId;
    }

    public String getLookUpKey() {
        return lookUpKey;
    }

    public String getPhoneType() {
        return phoneType;
    }
}
