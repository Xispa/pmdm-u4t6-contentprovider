package dam.androidignacio.u4t6contacts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnClickListener {

    MyContacts myContacts;
    RecyclerView recyclerView;
    TextView tvContactInfo;

    // Permission required to contacts provider, only needed to READ
    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};

    // Id to iderntify a contacts permission request
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if (checkPermissions()) {
            setListAdapter();
        }

    }

    private void setUI() {

        tvContactInfo = findViewById(R.id.tvContactInfo);

        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        // set recyclerView with a linear layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(divider);

    }

    private void setListAdapter() {
        // MyContacts class gets data from ContactsProvider
        myContacts = new MyContacts(this);
        // set adapter to recylcerView
        recyclerView.setAdapter(new MyAdapter(myContacts, this));

        // Hide empty list TextView
        if (myContacts.getCount() > 0) findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);

    }

    private boolean checkPermissions() {
        // check for permissions granted before setting listView Adapter data
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            // opens Dialog: requests user to grant permission
            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CONTACTS) {
            // We have requested one READ permission for contacts, so only need [0] to be checked.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setListAdapter();
            } else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onItemClickListener(ContactItem itemSelected) {

        String id = itemSelected.getId();
        String name = itemSelected.getName();
        String phoneNumber = itemSelected.getPhoneNumber();
        String contactId = itemSelected.getContactId();
        String rawContactId = itemSelected.getRawContactId();
        String lookupKey = itemSelected.getLookUpKey();
        String phoneType = itemSelected.getPhoneType();

        tvContactInfo.setText(name + " " + phoneNumber + " " + phoneType + System.lineSeparator() +
                " _ID: " + id + " CONTACT_ID: " + contactId + " RAW_CONTACT_ID: " + rawContactId + System.lineSeparator() +
                "LOOKUP_KEY: " + lookupKey);
    }
}