package dam.androidignacio.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

public class MyContacts {

    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context) {
        this.context = context;
        this.myDataSet = getContacts();
    }

    // Get contacts list from ContactsProvider
    private ArrayList<ContactItem> getContacts() {

        ArrayList<ContactItem> contactsList = new ArrayList<>();

        // access to ContentProviders
        ContentResolver contentResolver = context.getContentResolver();

        // aux variables  TODO Ex.1 Modificar consulta
        String[] projection = new String[] {ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI,};

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        // query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,    // URI to Contacts: content://com.android.contacts/data
                projection,                                                                 // projection
                selectionFilter,                                                            // selection filter
                null,                                                          // no selectionArgs
                ContactsContract.Data.DISPLAY_NAME + " ASC");                     // sortOrder

        if (contactsCursor != null) {
            // get the column indexes for desired Name and Number columns
            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int photoIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);
            int contactIdIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int rawContactIdIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int lookupKeyIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int phoneTypeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);

            // read data and add to ArrayList
            while (contactsCursor.moveToNext()) {


                String id = contactsCursor.getString(idIndex);
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String photo = contactsCursor.getColumnName(photoIndex);
                String contactId = contactsCursor.getString(contactIdIndex);
                String rawContactId = contactsCursor.getString(rawContactIdIndex);
                String lookupKey = contactsCursor.getString(lookupKeyIndex);
                String phoneType = contactsCursor.getString(phoneTypeIndex);

                Bitmap bitmap = null;
                try {
                    if (photo != null) {
                        bitmap = MediaStore.Images.Media.getBitmap(contentResolver, Uri.parse(photo));
                    }
                } catch (IOException e) {

                }

                contactsList.add(new ContactItem(id, name, number, bitmap,
                        contactId, rawContactId, lookupKey, phoneType));
            }
            contactsCursor.close();
        }

        return contactsList;
    }

    public ContactItem getContactsData(int position) {
        return myDataSet.get(position);
    }

    public int getCount() {
        return myDataSet.size();
    }

}
