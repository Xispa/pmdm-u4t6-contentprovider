package dam.androidignacio.u4t6contacts;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;
    private OnClickListener clickListener;

    // Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout constraintLayout;
        TextView contactId;
        TextView contactName;
        TextView contactPhoneNumber;
        ImageView contactImage;

        public MyViewHolder(ConstraintLayout view) {
            super(view);
            this.constraintLayout = view;
            contactId = view.findViewById(R.id.contactId);
            contactName = view.findViewById(R.id.contactName);
            contactPhoneNumber = view.findViewById(R.id.contactPhoneNumber);
            contactImage = view.findViewById(R.id.contactImage);
        }

        // sets viewHolder views with data
        public void bind(ContactItem contactData, OnClickListener clickListener) {
            this.contactId.setText(contactData.getId());
            this.contactName.setText(contactData.getName());
            this.contactPhoneNumber.setText(contactData.getPhoneNumber());
            this.contactImage.setImageBitmap(contactData.getImage());

            this.constraintLayout.setOnClickListener(v -> clickListener.onItemClickListener(contactData));

        }

    }

    // constructor: myContacts contains Contacts data
    MyAdapter(MyContacts myContacts, OnClickListener listener) {
        this.myContacts = myContacts;
        this.clickListener = listener;
    }

    public interface OnClickListener {
        void onItemClickListener(ContactItem itemSelected);
    }

    // Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create item View:
        // use a simple TextView predefined layout (sdk/latforms/android-xx/data/layout) that contains only TextView
        ConstraintLayout cl = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);

        return new MyViewHolder(cl);
    }

    // replaces the data content of a viewholder (recylces old viewholder): Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind viewHolder with data at: position
        viewHolder.bind(myContacts.getContactsData(position), clickListener);
    }

    // returns the size of dataSet: Layout Manager calls this method
    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }

}
